/*!
 * Javascript code based on the original toc.js library.
 * Original code has been heavily modified and refactored.
 *
 * toc - jQuery Table of Contents Plugin
 * http://projects.jga.me/toc/
 * copyright Greg Allen 2014
 * MIT License
 */
(($) => {
  const verboseIdCache = {};
  $.fn.toc = function fnToc(options) {
    const self = this;
    const opts = $.extend({}, jQuery.fn.toc.defaults, options);

    const $container = $(opts.container);
    const $headings = $(opts.selectors, $container);
    const activeClassName = opts.activeClass;

    // The Drupal admin toolbar adds a padding-top to the page body.
    // We try to handle it as properly as possible.
    let bodyPaddingTop = 0;
    function refreshBodyPaddingTop() {
      const bodyCompStyle = window.getComputedStyle(document.body);
      bodyPaddingTop = parseInt(
        bodyCompStyle.getPropertyValue('padding-top'),
        10,
      );
      document.dispatchEvent(new Event('padding-top-update'));
    }
    const mutObserver = new MutationObserver((mutations) => {
      mutations.forEach((mutationRecord) => {
        if (
          mutationRecord.type === 'attributes' &&
          mutationRecord.attributeName === 'style' &&
          mutationRecord.target === document.body
        ) {
          refreshBodyPaddingTop();
        }
      });
    });
    mutObserver.observe(document.body, {
      attributes: true,
      attributeFilter: ['style'],
    });

    function refreshHighlight() {
      const scrollHeight = window.innerHeight - bodyPaddingTop;
      let closest = Number.MAX_VALUE;
      let index = -1;
      for (let i = 0; i < $headings.length; i += 1) {
        const headingRect = $headings[i].getBoundingClientRect();
        const headingScrollTop = headingRect.top - bodyPaddingTop;
        const currentClosest = Math.abs(headingScrollTop);
        if (currentClosest < closest) {
          closest = currentClosest;
          index = i;
          if (headingScrollTop > scrollHeight / 2) {
            // Go back to the previous index if not high enough on screen.
            index = i - 1;
          }
        } else {
          break;
        }
      }
      if (index >= 0) {
        $('li', self).removeClass(activeClassName);
        const highlighted = $(`li:eq(${index})`, self);
        highlighted.addClass(activeClassName);
        opts.onHighlight(highlighted);
      }
    }

    // Highlight on scroll.
    let highlightTimeout;
    const highlightOnScroll = () => {
      if (highlightTimeout) {
        clearTimeout(highlightTimeout);
      }
      highlightTimeout = setTimeout(() => {
        refreshHighlight();
      }, 50);
    };

    if (opts.highlightOnScroll) {
      $(window).on('scroll', highlightOnScroll);
      highlightOnScroll();
    }

    let scrollToTimeout;
    const scrollTo = (e, anchor) => {
      if (opts.smoothScrolling) {
        e.preventDefault();
        if (scrollToTimeout) {
          clearTimeout(scrollToTimeout);
        }
        if (opts.highlightOnScroll) {
          $(window).off('scroll', highlightOnScroll);
        }
        anchor.scrollIntoView({ behavior: 'smooth' });
        window.history.replaceState(undefined, undefined, `#${anchor.id}`);
        if (opts.highlightOnScroll) {
          scrollToTimeout = setTimeout(() => {
            $(window).on('scroll', highlightOnScroll);
          }, 1000);
        }
      }
    };

    return this.each(function eachToc() {
      // build TOC
      const tocEl = this;
      const $tocEl = $(tocEl);

      let currentLevel = 0;
      let currentList;
      let lastLi = $tocEl.find('nav')[0];

      $headings.each((i, heading) => {
        const anchorName = opts.anchorName(i, heading, opts.prefix);

        // Add id to header to allow use as anchor.
        if (heading.id !== anchorName) {
          heading.id = anchorName;
        }
        // If specified, allow heading to take focus.
        if (opts.headingFocus) {
          heading.tabIndex = 0;
        }

        // build TOC item
        const a = document.createElement('a');
        a.textContent = opts.headerText(i, heading);
        a.href = `#${anchorName}`;
        a.addEventListener('click', (event) => {
          scrollTo(event, heading);
          if (opts.headingFocus) {
            heading.focus({ preventScroll: true });
          }
          $('li', self).removeClass(activeClassName);
          $(event.target).parent().addClass(activeClassName);
          $tocEl.trigger('selected', event.target.href);
        });

        const li = document.createElement('li');
        li.classList.add(opts.itemClass(i, heading, opts.prefix));
        li.append(a);

        const level = heading.tagName.slice(-1);

        if (level === currentLevel) {
          currentList.appendChild(li);
          lastLi = li;
        } else if (level > currentLevel) {
          currentLevel = level;
          const ul = document.createElement(opts.listType);
          ul.level = level;
          ul.appendChild(li);
          lastLi.appendChild(ul);
          currentList = ul;
          lastLi = li;
        } else if (level < currentLevel) {
          while (level < currentLevel) {
            currentList = currentList.parentNode.parentNode;
            currentLevel = currentList.level;
          }
          currentList.appendChild(li);
          lastLi = li;
          currentLevel = level;
        }
      });

      if ((opts.backToTop || opts.backToToc) && $headings.length > 1) {
        const tocId = $tocEl.attr('id');
        const firstHeaderId = $headings[0].id;
        const firstLinkSelector = `a[href="#${firstHeaderId}"]`;
        const firstLink = $(firstLinkSelector, self);
        $tocEl.find('a').each(function updateTocLinks(index) {
          const link = this;
          const linkHash = link.hash;
          const anchor = linkHash.replace(/^#/, '');
          const header = document.getElementById(anchor);
          if (opts.backToTop && index > 0) {
            const a = document.createElement('a');
            a.textContent = opts.backToTopLabel;
            a.href = `#${$headings[0].id}`;
            a.addEventListener('click', (e) => {
              const heading = document.getElementById($headings[0].id);
              scrollTo(e, heading);
              if (opts.headingFocus) {
                heading.focus({ preventScroll: true });
              }
              $('li', self).removeClass(opts.activeClass);
              firstLink.parent().addClass(opts.activeClass);
              $tocEl.trigger('selected', linkHash);
            });
            $(a).addClass(opts.backToTopClass).insertBefore($(header));
          }
          if (opts.backToToc) {
            const anchorNameToToc = `${tocId}-${anchor}-to-toc`;
            $(link).attr('id', anchorNameToToc);
            const a = document.createElement('a');
            a.textContent = opts.backToTocLabel;
            a.href = `#${anchorNameToToc}`;
            a.addEventListener('click', (e) => {
              scrollTo(e, link);
              link.focus({ preventScroll: true });
              $('li', self).removeClass(opts.activeClass);
              $(link).parent().addClass(opts.activeClass);
              $tocEl.trigger('selected', linkHash);
            });
            $(a).addClass(opts.backToTocClass).insertBefore($(header));
          }
        });
      }

      // Stick the toc.
      function stickTheToc() {
        tocEl.style.top = `${opts.stickyOffset + bodyPaddingTop}px`;
      }
      if ($tocEl.hasClass('sticky') && opts.stickyOffset >= 0) {
        document.addEventListener('padding-top-update', () => {
          stickTheToc();
        });
        refreshBodyPaddingTop();
      }

      const n = $tocEl.find('li').length;
      const minimum = opts.selectorsMinimum;
      if (n >= minimum) {
        $tocEl.show();
        $container.find('a.back-to-top').show();
      }
    });
  };

  jQuery.fn.toc.defaults = {
    container: 'body',
    selectorsMinimum: 0,
    listType: 'ul',
    selectors: 'h1,h2,h3',
    smoothScrolling: true,
    stickyOffset: 0,
    prefix: 'toc',
    activeClass: 'toc-active',
    onHighlight() {},
    highlightOnScroll: true,
    highlightOffset: 0,
    backToTop: false,
    headingFocus: false,
    backToToc: false,
    anchorName(i, heading, prefix) {
      if (heading.id.length) {
        return heading.id;
      }
      let candidateId = heading.textContent
        .toLowerCase()
        .replace(/[^a-z0-9]/gi, ' ')
        .replace(/\s+/g, '-');
      if (verboseIdCache[candidateId]) {
        let j = 2;
        while (verboseIdCache[candidateId + j]) {
          j += 1;
        }
        candidateId = `${candidateId}-${j}`;
      }
      verboseIdCache[candidateId] = true;
      return prefix ? `${prefix}-${candidateId}` : candidateId;
    },
    headerText(i, heading) {
      return heading.textContent;
    },
    itemClass(i, heading, prefix) {
      const tagName = heading.tagName.toLowerCase();
      return prefix ? `${prefix}-${tagName}` : tagName;
    },
  };
})(jQuery);
